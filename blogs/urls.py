from django.urls import path
from blogs.views import list_posts
urlpatterns = [
    path("", list_posts, name="post_list")
]